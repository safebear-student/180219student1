package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.LoginPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class LoginPage {
    LoginPageLocators locatorsx = new LoginPageLocators();
    @NonNull
    WebDriver driver;

// login pages do ACTIONS on
    public String getPageTitle (){
        return driver.getTitle();
    }

    public void enterUsername(String username){
        driver.findElement(locatorsx.getUsernameLocator()).sendKeys(username);
    }

    public void enterPassword(String password){
        driver.findElement(locatorsx.getPasswordLocator()).sendKeys(password);
    }

    public void clickLoginButton(){
        driver.findElement(locatorsx.getLoginLocator()).click();
    }
}

