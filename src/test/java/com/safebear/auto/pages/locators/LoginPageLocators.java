package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;
// the @Data will automatically set the getters and setters. So i dont need to type in the getters and setters
@Data
public class LoginPageLocators {
    private By usernameLocator = By.id("username");
    private By passwordLocator = By.id("password");
    private By loginLocator = By.id("enter");
}
