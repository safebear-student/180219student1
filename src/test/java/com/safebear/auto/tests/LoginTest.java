package com.safebear.auto.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import utils.Utils;

import java.util.List;

// LoginTest where you do your enter data and do assertions
public class LoginTest extends BaseTest {
    @Test
    public void loginIntoTest() {
        List<List<String>> rows = Utils.getCsvData("src/test/resources/users.csv");
        for (List<String> row: rows){
            driver.get(Utils.getUrl());
            Assert.assertEquals(loginPage.getPageTitle(),"Login Page", "the login page did not open");
            loginPage.enterUsername(row.get(0));
            loginPage.enterPassword(row.get(1));
            loginPage.clickLoginButton();
            Assert.assertEquals(toolsPage.getPageTitle(),"Tools Page", "the login page did not opens");
        }
       // driver.get(Utils.getUrl());
      //  Assert.assertEquals(loginPage.getPageTitle(), "Login Page", "The login page did not open, or the title has changed");
      //  loginPage.enterUsername("tester");
      //  loginPage.enterPassword("letmein");
      //  loginPage.clickLoginButton();
      //  Assert.assertEquals(toolsPage.getPageTitle(), "Tools Page", "the login page did not open or the text has changed");
    }
// do you unsccessfull tests.
}
